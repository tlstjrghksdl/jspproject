package domain;

public class VotePeople {
	private int num;
	private int ages;
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getAges() {
		return ages;
	}
	public void setAges(int ages) {
		this.ages = ages;
	}
	public VotePeople(int num, int ages) {
		this.num = num;
		this.ages = ages;
	}
	
	
}
