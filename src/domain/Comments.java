package domain;

public class Comments {
	private int colevel;
	private int rootid;
	private String comment;
	private String writer;
	
	public int getColevel() {
		return colevel;
	}
	public void setColevel(int colevel) {
		this.colevel = colevel;
	}
	public int getRootid() {
		return rootid;
	}
	public void setRootid(int rootid) {
		this.rootid = rootid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	
} 
