package domain;

public class Notif {
	private String id;
	private String writer;
	private String title;
	private String date;
	private String content;
	private String rootid;
	private String relevel;
	private String recnt;
	private String viewcnt;

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getRootid() {
		return rootid;
	}

	public void setRootid(String titlenum) {
		this.rootid = titlenum;
	}

	public String getRelevel() {
		return relevel;
	}

	public void setRelevel(String relevel) {
		this.relevel = relevel;
	}

	public String getRecnt() {
		return recnt;
	}

	public void setRecnt(String recnt) {
		this.recnt = recnt;
	}

	public String getViewcnt() {
		return viewcnt;
	}

	public void setViewcnt(String viewcnt) {
		this.viewcnt = viewcnt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
