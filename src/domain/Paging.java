package domain;

public class Paging {
    private int onePageNum; // 한 페이지에 표시할 글 수
    private int firstPageNo; // 첫 번째 페이지 번호(1, 11, 21, 31)
    private int prevPageNo; // 이전 페이지 번호
    private int startPageNo; // 시작 페이지 (페이징 네비 기준)
    private int nowPageNo; // 페이지 번호(현재)
    private int endPageNo; // 끝 페이지 (페이징 네비 기준)
    private int nextPageNo; // 다음 페이지 번호
    private int finalPageNo; // 마지막 페이지 번호
    private int totalCount; // 게시 글 전체 수

 
    public int getPageSize() {
        return onePageNum;
    }

    public void setPageSize(int pageSize) {
        this.onePageNum = pageSize;
    }

    public int getFirstPageNo() {
        return firstPageNo;
    }
    
    public void setFirstPageNo(int firstPageNo) {
        this.firstPageNo = firstPageNo;
    }

    public int getPrevPageNo() {
        return prevPageNo;
    }

    public void setPrevPageNo(int prevPageNo) {
        this.prevPageNo = prevPageNo;
    }

    public int getStartPageNo() {
        return startPageNo;
    }

    public void setStartPageNo(int startPageNo) {
        this.startPageNo = startPageNo;
    }

    public int getPageNo() {
        return nowPageNo;
    }

    public void setPageNo(int pageNo) {
        this.nowPageNo = pageNo;
    }

    public int getEndPageNo() {
        return endPageNo;
    }

    public void setEndPageNo(int endPageNo) {
        this.endPageNo = endPageNo;
    }

    public int getNextPageNo() {
        return nextPageNo;
    }

    public void setNextPageNo(int nextPageNo) {
        this.nextPageNo = nextPageNo;
    }

    public int getFinalPageNo() {
        return finalPageNo;
    }

    public void setFinalPageNo(int finalPageNo) {
        this.finalPageNo = finalPageNo;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        this.makePaging();
    }

    private void makePaging() {
        if (this.totalCount == 0) return; // 게시 글 전체 수가 없는 경우
        if (this.nowPageNo == 0) this.setPageNo(1); // 기본 값 설정
        if (this.onePageNum == 0) this.setPageSize(10); // 기본 값 설정

        int finalPage = (totalCount + (onePageNum - 1)) / onePageNum; // 마지막 페이지
        if (this.nowPageNo > finalPage) this.setPageNo(finalPage); // 기본 값 설정

        if (this.nowPageNo < 0 || this.nowPageNo > finalPage) this.nowPageNo = 1; // 현재 페이지 유효성 체크

        boolean isNowFirst = nowPageNo == 1 ? true : false; // 시작 페이지 (전체)
        boolean isNowFinal = nowPageNo == finalPage ? true : false; // 마지막 페이지 (전체)

        int startPage = ((nowPageNo - 1) / 10) * 10 + 1; // 시작 페이지 (페이징 네비 기준)
        int endPage = startPage + 10 - 1; // 끝 페이지 (페이징 네비 기준)

        if (endPage > finalPage) { // [마지막 페이지 (페이징 네비 기준) > 마지막 페이지] 보다 큰 경우
            endPage = finalPage;
        }

        this.setFirstPageNo(1); // 첫 번째 페이지 번호

        if (isNowFirst) {
            this.setPrevPageNo(1); // 이전 페이지 번호
        } else {
            this.setPrevPageNo(((nowPageNo - 1) < 1 ? 1 : (nowPageNo - 1))); // 이전 페이지 번호
        }

        this.setStartPageNo(startPage); // 시작 페이지 (페이징 네비 기준)
        this.setEndPageNo(endPage); // 끝 페이지 (페이징 네비 기준)

        if (isNowFinal) {
            this.setNextPageNo(finalPage); // 다음 페이지 번호
        } else {
            this.setNextPageNo(((nowPageNo + 1) > finalPage ? finalPage : (nowPageNo + 1))); // 다음 페이지 번호
        }

        this.setFinalPageNo(finalPage); // 마지막 페이지 번호
    }
}


