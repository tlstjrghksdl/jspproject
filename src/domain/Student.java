package domain;

import java.text.DecimalFormat;

public class Student {
	private String name;
	private String id;
	private String kor;
	private String eng;
	private String mat;
	private String sum;
	private String avg;
	private String rank;
	
	
	
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getAvg() {
		return avg;
	}
	public void setAvg(String avg) {
		this.avg = avg;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKor() {
		return kor;
	}
	public void setKor(String kor) {
		this.kor = kor;
	}
	public String getEng() {
		return eng;
	}
	public void setEng(String eng) {
		this.eng = eng;
	}
	public String getMat() {
		return mat;
	}
	public void setMat(String mat) {
		this.mat = mat;
	}
	
	public int sum(int kor, int eng, int mat) {
		return kor+eng+mat;
	}
	
	public double avg(int sum) {
		DecimalFormat df = new DecimalFormat("0.##");
		String Savg = df.format(sum/3);
		double avg = Double.parseDouble(Savg);
		return avg;
	}
}
