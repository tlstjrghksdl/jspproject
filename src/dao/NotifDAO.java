package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Comments;
import domain.Notif;
import domain.UploadFile;

public class NotifDAO {
	// DB와 연결할 때 필요한 친구들
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;

	// 연결 메소드
	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://192.168.56.1:33061/notification?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";

			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	// 연결중지 메소드
	public static void disconnect() {
		try {
			pstmt.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// selectAll method
	public static List<Notif> selectAll() {
		List<Notif> notices = new ArrayList<>();
		connect();
		try {
			// order by를 얼마나 잘쓰냐~에 따라서 순서가 정해진다.
			rs = stmt.executeQuery("select * from notice order by rootid asc, recnt desc, relevel desc;");
			while (rs.next()) {
				Notif notice = new Notif();
				notice.setId(rs.getString(1));
				notice.setWriter(rs.getString(2));
				notice.setTitle(rs.getString(3));
				notice.setDate(rs.getString(4));
				notice.setContent(rs.getString(5));
				notice.setRootid(rs.getString(6));
				notice.setRelevel(rs.getString(7));
				notice.setRecnt(rs.getString(8));
				notice.setViewcnt(rs.getString(9));

				notices.add(notice);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return notices;
	}

	public static Notif selectOne(int id) {
		Notif notice = new Notif();
		connect();
		try {

			rs = stmt.executeQuery("select * from notice where id=" + id + ";");
			if (rs.next()) {
				notice.setId(rs.getString(1));
				notice.setWriter(rs.getString(2));
				notice.setTitle(rs.getString(3));
				notice.setDate(rs.getString(4));
				notice.setContent(rs.getString(5));
				notice.setRootid(rs.getString(6));
				notice.setRelevel(rs.getString(7));
				notice.setRecnt(rs.getString(8));
				notice.setViewcnt(rs.getString(9));
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return notice;
	}

	public static List<UploadFile> selectFile(int id) {
		List<UploadFile> ufs = new ArrayList<>();
		connect();
		try {
			rs = stmt.executeQuery("select * from file where rootid =" + id + ";");
			while (rs.next()) {
				UploadFile uf = new UploadFile();
				uf.setId(rs.getString(1));
				uf.setFilename(rs.getString(2));
				ufs.add(uf);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return ufs;
	}

	public static void deleteNotice(int id) {
		connect();
		try {

			pstmt = conn.prepareStatement("delete from notice where id=?;");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

			pstmt = conn.prepareStatement("delete from notice where rootid=?;");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			
			pstmt = conn.prepareStatement("delete from file where rootid=?;");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			
			pstmt = conn.prepareStatement("delete from comment where rootid=?;");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}

	public static int getNum() {
		int num = 0;
		connect();
		try {

			rs = stmt.executeQuery("select max(id) from notice;");
			if (rs.next()) {
				num = Integer.parseInt(rs.getString(1));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return num;
	}

	public static int getRecnt(String rootid, String relevel) {
		int num = 0;
		connect();
		try {
			rs = stmt.executeQuery(
					"select max(recnt)+1 from notice where rootid=" + rootid + " and relevel=" + relevel + ";");
			if (rs.next()) {
				num = rs.getInt(1);
			}
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return num;
	}

	public static void updateViewCnt(int num) {
		connect();
		try {

			pstmt = conn.prepareStatement("update notice set viewcnt = viewcnt+1 where id=?;");
			pstmt.setInt(1, num);
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}

	public static String getRelevelMax(String id) {
		String num = "";
		connect();
		try {

			rs = stmt.executeQuery("select max(relevel)+1 from notice where rootid=" + id + ";");
			if (rs.next()) {
				num = rs.getString(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return num;
	}

	public static void insertOrUpdate(String id, String writer, String title, String date, String content,
			String rootid, String relevel, String recnt, String viewcnt) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into notice values(?, ?, ?, ?, ?, ?, ? ,?, ?)"
					+ " on duplicate key update title=?, content=?;");
			pstmt.setString(1, id);
			pstmt.setString(2, writer);
			pstmt.setString(3, title);
			pstmt.setString(4, date);
			pstmt.setString(5, content);
			pstmt.setString(6, rootid);
			pstmt.setString(7, relevel);
			pstmt.setString(8, recnt);
			pstmt.setString(9, viewcnt);
			pstmt.setString(10, title);
			pstmt.setString(11, content);

			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();

	}
	
	// 파일 업로드 메소드
	public static void insertFile(String id, String filename) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into file values(?, ?);");
			pstmt.setString(1, id);
			pstmt.setString(2, filename);

			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}

	// 검색 메소드
	public static List<Notif> search(String searchTitle, String search) {
		List<Notif> notices = new ArrayList<>();
		connect();
		try {
			//검색 타이틀과, 검색어를 쿼리문을 통해 찾아서 List로 return한다.
			rs = stmt.executeQuery("select * from notice where " + searchTitle + " like '%" + search
					+ "%' order by rootid asc, recnt desc, relevel desc;");
			while (rs.next()) {
				Notif notice = new Notif();
				notice.setId(rs.getString(1));
				notice.setWriter(rs.getString(2));
				notice.setTitle(rs.getString(3));
				notice.setDate(rs.getString(4));
				notice.setContent(rs.getString(5));
				notice.setRootid(rs.getString(6));
				notice.setRelevel(rs.getString(7));
				notice.setRecnt(rs.getString(8));
				notice.setViewcnt(rs.getString(9));

				notices.add(notice);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return notices;
	}
	
	// 댓글 메소드
	public static void insertComment(String rootid, String comment, String writer) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into comment (rootid, comment, writer) values(?, ?, ?);");
			pstmt.setString(1, rootid);
			pstmt.setString(2, comment);
			pstmt.setString(3, writer);

			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}
	
	public static List<Comments> selectComment(String rootid) {
		List<Comments> comments = new ArrayList<>();
		connect();
		try {

			rs = stmt.executeQuery("select * from comment where rootid=" + rootid + " order by colevel desc;");
			while (rs.next()) {
				Comments comment = new Comments();
				comment.setColevel(rs.getInt(1));
				comment.setRootid(rs.getInt(2));
				comment.setComment(rs.getString(3));
				comment.setWriter(rs.getString(4));
				comments.add(comment);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		return comments;
	}

	public static void deleteFile(String fileName) {
		
		
	}
}