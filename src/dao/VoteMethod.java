package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Candidate;
import domain.VotePeople;

public class VoteMethod {
	
	// DB와 연결할 때 필요한 친구들
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;
	
	
	// 연결 메소드
	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			String url = "jdbc:mysql://192.168.56.1:33061/vote?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";

			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	// 연결중지 메소드
	public static void disconnect() {
		try {
			pstmt.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	// 후보자들 모두 보여지는 메소드. 모두 ui로 보여줘야하기 때문에 list에 객체를 담아서 return한다.
	public static List<Candidate> selectAll() {
		List<Candidate> cdds = new ArrayList<>();
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from candidate;");
			while (rs.next()) {
				Candidate cdd = new Candidate();
				cdd.setNum(rs.getInt(1));
				cdd.setName(rs.getString(2));
				cdds.add(cdd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return cdds;
	}

	
	// 후보자 등록하는 메소드. 후보자 객체를 받아와서 그 객체가 소유한 속성을 insert한다.
	public static void insertCandidate(Candidate candidate) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into candidate values(?, ?);");
			pstmt.setInt(1, candidate.getNum());
			pstmt.setString(2, candidate.getName());

			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();

	}
	
	// 후보자 삭제 메소드. 후보자 객체를 받아와서 그 객체가 소유한 속성을 delete한다.
	public static void deleteCandidate(Candidate candidate) {

		connect();
		try {
			pstmt = conn.prepareStatement("delete from candidate where id=?");
			pstmt.setInt(1, candidate.getNum());
			pstmt.execute();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}
	
	// 투표를 하는 메소드. 투표 객체를 받아와 그 속성을 insert한다.
	public static void insertVote(VotePeople vp) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into votepaper values(?, ?);");
			pstmt.setInt(1, vp.getNum());
			pstmt.setInt(2, vp.getAges());

			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}
	
	// 투표 현황 메소드. 후보자 객체를 받아와서 후보자의 번호로 투표결과 table에서 count한다. 
	public static int viewResult(Candidate candidate) {
		int num = 0;
		connect();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select count(*) from votepaper where id="+ candidate.getNum() +";");
			if(rs.next()) {
			num = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return num;
		
	}
	
	// 투표 현황에서 후보자를 클릭했을때, 연령대별 투표수를 보여주는 메소드.
	// 눌렀을때 가져오는 후보자의 번호로 count한다.
	public static int viewResultOne(int num, int age) {
		int rtNum = 0;
		connect();
		try {
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("select count(*) from votepaper where id="+ num +" and age=" + age + ";");
			if(rs.next()) {
			rtNum = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return rtNum;
		
	}

}
