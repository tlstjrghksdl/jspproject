package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Student;

public class StudentDAO {
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement pstmt = null;

	public static void connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		
			String url = "jdbc:mysql://192.168.56.1:33061/twice?"
					+ "serverTimezone=UTC&characterEncoding=UTF-8&allowPublicKeyRetrieval=true&useSSL=false";
	
			conn = DriverManager.getConnection(url, "root", "kopo36");
			stmt = conn.createStatement();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	public static void disconnect() {
		try {
			stmt.close();
			conn.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static String getBlankNum() {
		String num = "";
		connect();
		try {
			String sql = "select min(id+1) from twice where id+1 not in (select id from twice);";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String a = rs.getString(1);
				num = a;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return num;
	}
	
	
	public static void insertDB(Student st) {
		connect();
		try {
			pstmt = conn.prepareStatement("insert into twice values(?, ?, ?, ?, ?);");
			pstmt.setString(1, st.getName());
			pstmt.setString(2, st.getId());
			pstmt.setString(3, st.getKor());
			pstmt.setString(4, st.getEng());
			pstmt.setString(5, st.getMat());
			
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	
	}

	public static void drop() {
		connect();
		try {
			stmt.executeQuery("drop table twice;");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}

	
	public static void create() {
		connect();
		try {
			stmt.executeQuery("create table twice(name varchar(20), id varchar(20), kor int, eng int, mat int);");		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}

	public static List<Student> selectAll() {
		List<Student> students = new ArrayList<>();
		connect();
		try {
			rs = stmt.executeQuery("select name, id, kor, eng, mat, (kor+eng+mat) as sum, "
					+ "(kor+eng+mat)/3 as avg, rank() over(order by (kor+eng+mat) desc)"
					+ " from twice order by id;");
			while (rs.next()) {
				Student std = new Student();
				std.setName(rs.getString(1));
				std.setId(rs.getString(2));
				std.setKor(rs.getString(3));
				std.setEng(rs.getString(4));
				std.setMat(rs.getString(5));
				std.setSum(rs.getString(6));
				std.setAvg(rs.getString(7));
				std.setRank(rs.getString(8));
				students.add(std);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return students;
	}
	

	public static Student selectOne(String id) {
		Student std = new Student();
		connect();
		try {
			rs = stmt.executeQuery("select * from twice where id='" + id + "';");
			if (rs.next()) {
				std.setName(rs.getString("name"));
				std.setId(rs.getString("id"));
				std.setKor(rs.getString("kor"));
				std.setEng(rs.getString("eng"));
				std.setMat(rs.getString("mat"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		disconnect();
		return std;
	}
	
	public static void update(Student student) {
		connect();
		try {
			pstmt = conn.prepareStatement("update twice set name=?, kor=?, eng=?, mat=? where id=?;");
			pstmt.setString(1, student.getName());
			pstmt.setString(2, student.getKor());
			pstmt.setString(3, student.getEng());
			pstmt.setString(4, student.getMat());
			pstmt.setString(5, student.getId());
			pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
		
	}

	public static void delete(Student student) {
		
		connect();
		try {
			/* 紐⑤몢 蹂댁뿬 二쇰뒗 寃껋씠湲� �븣臾몄뿉 select * from twice瑜� �븳�떎. */
			pstmt = conn.prepareStatement("delete from twice where id=?");
			pstmt.setString(1, student.getId());
			pstmt.execute();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		disconnect();
	}
	
	
	public static List<String> selectAllRank() {
		List<String> rank = new ArrayList<>();
		connect();
		try {
			rs = stmt.executeQuery("select rank() over(order by (kor+eng+mat) desc) from twice order by id;");
			while (rs.next()) {
				rank.add(rs.getString(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return rank;
	}
}
