package dao;

public class WifiTable {
	private String address;
	private String latitude;
	private String longitude;
	private double dists;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public double getDists() {
		return dists;
	}
	public void setDists(double dists) {
		this.dists = dists;
	}
	
	
	
}
