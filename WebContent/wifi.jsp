<%@page import="domain.Paging"%>
<%@page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@page import="dao.WifiTable"%>
<%@ page contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
</style>
</head>

<body>
	<%-- 페이징에 필요한 변수 선언 --%>
	<%
		// 상대경로로 텍스트 파일 불러오기
	File f = new File("C:\\Users\\tjrgh\\Desktop\\wifiutf.csv");
	BufferedReader br = new BufferedReader(new FileReader(f));
	String readtxt;
	if ((readtxt = br.readLine()) == null) {
		out.println("빈 파일입니다\n");
		return;
	}
	// 현재 나의 위도, 경도 지정
	double lat = 37.3860521;
	double lng = 127.1214038;
	// 한줄씩 읽을 때마다 count할 변수
	int LineCnt = 0;
	// 주소 값을 저장할 ArrayList 선언
	List<WifiTable> wfts = new ArrayList<>();
	// 텍스트 파일을 한 줄 씩 읽으면서
	while ((readtxt = br.readLine()) != null) {
		WifiTable wifitable = new WifiTable();
		// 탭으로 구분하여 String형 배열 field에 저장
		String[] field = readtxt.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		// 9번째 필드 값을 address에 저장
		wifitable.setAddress(field[9]);
		// 12번째 필드 값을 latitude에 저장
		wifitable.setLatitude(field[12]);
		// 13번째 필드 값을 longtitude에 저장
		wifitable.setLongitude(field[13]);
		// 내 위치와의 거리 구하기
		double dist = Math
		.sqrt(Math.pow(Double.parseDouble(field[12]) - lat, 2) + Math.pow(Double.parseDouble(field[13]) - lng, 2));
		// dist 값을 dists에 저장
		wifitable.setDists(dist);
		// 한 줄 씩 읽을때마다 count
		wfts.add(wifitable);
		LineCnt++;
	}
	
	Paging p = new Paging();

	p.setTotalCount(LineCnt);
	int cpage = request.getParameter("pg") != null ? Integer.parseInt(request.getParameter("pg")) : 1;
	int prevPage = (int) Math.floor((cpage - 1) / p.getPageSize()) * p.getPageSize();
	int nextPage = prevPage + p.getPageSize() + 1;
	p.setPageNo(cpage);
	p.setPrevPageNo(prevPage);
	p.setNextPageNo(nextPage);

	
	// url에서 from의 값을 받아서 from에 저장
	String from = request.getParameter("from");
	// url에서 cnt의 값을 받아서 cnt에 저장 
	String cnt = request.getParameter("cnt");

	// url에서 받아온 값을 int로 변환하여 저장할 변수
	int fromPT;
	int cntPT;

	// url에서 from이나 cnt의 값이 없으면 (첫페이지 처리 위함)
	if (from == null || cnt == null) {
		// fromPT=0, cntPT=10인 것으로 판단
		fromPT = 0;
		cntPT = 10;
	} else {
		if(cpage == p.getFinalPageNo()) {
			fromPT = Integer.parseInt(from);
			cntPT = LineCnt % 10;				
		}
		fromPT = Integer.parseInt(from);
		cntPT = Integer.parseInt(cnt);
	}
	out.println(p.getFinalPageNo());
	%>
		<!-- /* i가 fromPT~ fromPT+cntPT 사이에 있는 동안 
		from번째 부터 cnt만큼의 데이터를 출력하기 위함 */ -->
		
	<table border=1 width=900px
		style='margin-left: auto; margin-right: auto; table-layout: fixed;'>
		<tr align=center>
			<td width=50px>번호</td>
			<td width=400px>주소</td>
			<td>위도</td>
			<td>경도</td>
			<td width=180px>거리</td>
		</tr>
		<% for (int i = fromPT; i < fromPT + cntPT; i++) {%>
		<tr align=center>
			<td><%=i + 1%></td>
			<td align=left><%=wfts.get(i).getAddress()%></td>
			<td><%=wfts.get(i).getLatitude()%></td>
			<td><%=wfts.get(i).getLongitude()%></td>
			<td><%=wfts.get(i).getDists()%></td>
		</tr>
		<% } %>
	</table>

	<table style='margin-left: auto; margin-right: auto; table-layout: fixed;'>
		<tr>
			<td align="center">
				<%
				if (p.getNextPageNo() > p.getFinalPageNo()) {
	
					if (p.getPageNo() / 11 != 0) {%> 
					<a href="wifi.jsp"><<</a> 
					<a href="wifi.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=p.getPrevPageNo() * 10%>&cnt=10"><</a> <%
					}
					
					for (int i = 1 + p.getPrevPageNo(); i <= p.getFinalPageNo() - 1; i++) {%>
				 		<a href="wifi.jsp?pg=<%=i%>&from=<%=(i * 10 - 10)%>&cnt=10"> <%=i%> </a> 
				 	<%} %>
						<a href="wifi.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=<%=LineCnt % 10%>"><%=p.getFinalPageNo() %></a>
				 	<%
				} else {
					
					if (p.getPageNo() / 11 != 0) {%> 
						<a href="wifi.jsp"><<</a> 
						<a href="wifi.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=p.getPrevPageNo() * 10%>&cnt=10"><</a>
					<% }
				 	
					for (int i = 1 + p.getPrevPageNo(); i < p.getNextPageNo(); i++) {%> 
						<a href="wifi.jsp?pg=<%=i%>&from=<%=(i * 10 - 10)%>&cnt=10"> <%=i%> </a> 
					<%}
					if (p.getPageNo() != p.getFinalPageNo()) {%> 
					<a href="wifi.jsp?pg=<%=p.getNextPageNo()%>&from=<%=p.getNextPageNo() * 10%>&cnt=10">></a>
					<a href="wifi.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=<%=LineCnt % 10%>">>></a>
					<%}
				}%>
			</td>
		</tr>
	</table>
</body>
</html>
