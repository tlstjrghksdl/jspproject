<%@page import="domain.UploadFile"%>
<%@page import="dao.NotifDAO"%>
<%@page import="domain.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import = "com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@ page import = "com.oreilly.servlet.MultipartRequest" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-bottom: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>

<script type="text/javascript">

	function goReply() {
		var del = document.forms['inputform'];
		del.action = 'replyNotice.jsp';
		del.submit();
	}

	function goDelete() {
		var del = document.forms['inputform'];
		del.action = 'deleteCandidate.jsp';
		del.submit();
	}
</script>
</head>
<body>
	<%
		  
   		// 업로드한 폴더의 위치와 업로드 폴더의 이름을 알아야 한다.
  	 	String savePath ="upload";// WebContent/uploadFile
   		// 위의 폴더는 상대경로이고 절대경로 기준의 진짜 경로를 구해와야한다.
  		String uploadPath = request.getSession().getServletContext().getRealPath(savePath);		// 업로드 경로
		int maxFileSize = 1024 * 1024 * 2;	// 업로드 제한 용량 = 2MB
		String encoding = "utf-8";			// 인코딩
	
		MultipartRequest multi = new MultipartRequest(
			request, 
			uploadPath, 
			maxFileSize, 
			encoding, 
			new DefaultFileRenamePolicy());
		
		String id = multi.getParameter("id");
		String writer = multi.getParameter("writer");
		String title = multi.getParameter("title");
		String date = multi.getParameter("date");
		String content = multi.getParameter("content");
		String rootid = multi.getParameter("rootid");
		String relevel = multi.getParameter("relevel");
		String recnt = multi.getParameter("recnt");
		String viewcnt = multi.getParameter("viewcnt");
		String upload = multi.getFilesystemName("upload");

		StringBuffer sb = new StringBuffer();
		if(relevel != null) {
			for(int i = 0; i < Integer.parseInt(relevel); i++) {
				sb.append("-");
			}
		
			if(sb.length() > 0) {
				sb.append(">[답글]");
			} else{
			}
		}
		
		if(viewcnt == null) {
			viewcnt = "0";
		} else {
		}
		
		if(relevel == null || relevel =="") {
			relevel = "0";
		} else {
		}
		
		if(recnt == null) {
			recnt = "0"; 
		} else {
		}
		
		
		
		Notif notice = new Notif();
		notice.setId(id);
		notice.setWriter(writer);
		notice.setTitle(sb.toString());
		notice.setDate(date); 
		notice.setContent(content);
		notice.setRootid(id);
		notice.setRelevel(relevel);
		notice.setRecnt(recnt);
		notice.setViewcnt(viewcnt); 
		NotifDAO.insertOrUpdate(id, writer, sb + title, date, notice.getContent(), rootid, relevel, recnt, viewcnt);
		Notif noticeOne = NotifDAO.selectOne(Integer.parseInt(id));
		
		
		UploadFile uf = new UploadFile();
		if(upload != null) {
			uf.setId(id);
			uf.setFilename(upload);
			NotifDAO.insertFile(rootid, upload);
		} else {
			
		}
		
		List<UploadFile> ufs = NotifDAO.selectFile(Integer.parseInt(id));
		request.setAttribute("noticeOne", noticeOne);
		
	%>
	<div style="padding-top: 20px;">
		<table style="border-color: #FF337B">
			<tr>
				<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
				<td style="border-bottom-color: #FF337B" align="right">
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>2020년 1학기</option>
				</select>
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
				</select></td>
			</tr>
		</table>
	</div>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="updateNotice.jsp" name="inputform" onsubmit="return check()" enctype="multipart/form-data">
		<table>	
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
			<col width="8%">
		</colgroup>
			<tr>
				<th>년도/학기</th>
				<td colspan="5">2020년도 1학기</td>
			</tr>
			<tr>
				<th>강의</th><td colspan="5">웹서버프로그래밍</td>
			</tr>
			<tr>
				<th>제목</th>
				<td colspan="5"><input type="hidden" value="<%=title %>" name="title"><%=title %></td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><input type="hidden" value="${noticeOne.date}" name="date">${noticeOne.date}</td>
				<th>작성자</th>
				<td><input type="hidden" value="${noticeOne.writer}" name="writer">${noticeOne.writer}</td>
			</tr>
			<tr>
				<td colspan="6" height="250px;"><input type="hidden" value="${noticeOne.content}" name="content">${noticeOne.content}</td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td colspan="5" height="50px;">
					<%
					for(int i = 0; i < ufs.size(); i++) {
						if(ufs.get(i).getFilename() == null) {
							break;
						}%>
					<a href="filedown.jsp?file=<%=ufs.get(i).getFilename()%>"><%=ufs.get(i).getFilename()%></a><br>	
					<%} %>
				</td>
			</tr>
		</table>		
		<input type="hidden" value="${noticeOne.rootid}" name="rootid">
		<input type="hidden" value="${noticeOne.id}" name="id">
		<input type="hidden" value="${noticeOne.recnt}" name="recnt"/>
		<input type="hidden" value="${noticeOne.viewcnt}" name="viewcnt"/>
		<input type="hidden" value="${noticeOne.relevel}" name="content">
		<div align="right">
		<a href="mainNotice.jsp"><input type="button" id="button" style="background-color: gray;" value="목록"/></a>
		<a href="replyNotice.jsp"><input type="button" id="button" onclick="goReply();" value="답변"/></a>
		<input type="submit" id="button" value="수정"/>
		</div>
	</form>
</body>
</html>