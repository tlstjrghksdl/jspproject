<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="dao.NotifDAO"%>
<%@page import="domain.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-bottom: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

	
	// 뒤로 돌아가기
	function goBack() {
	    window.history.back();
	}

	// 제목의 공백을 잡아주는 함수
	function join(){

		var title = document.fr.title.value;

		var newtitle = title.replace(/(\s*)/g, "");

		if(newtitle.length == 0) {
			alert("공백입니다. 입력해주세요.")
			document.fr.title.focus();
			return false;
		}
	}
		
</script>
</head>
<body>
	<%		String uploadPath = "C:/workspaces/Notice/WebContent/upload";		// 업로드 경로
			int maxFileSize = 1024 * 1024 * 2;	// 업로드 제한 용량 = 2MB
			String encoding = "utf-8";			// 인코딩

			MultipartRequest multi = new MultipartRequest(
				request, 
				uploadPath, 
				maxFileSize, 
				encoding, 
			new DefaultFileRenamePolicy());
			
			// 전 jsp에서 받아온 파라미터값들을 변수에 담는다.
			String id = multi.getParameter("id");
			String writer = multi.getParameter("writer");
			String title = multi.getParameter("title");
			String date = multi.getParameter("date");
			String content = multi.getParameter("content");
			String viewcnt = multi.getParameter("viewcnt");
			String rootid = multi.getParameter("rootid");
			
			// notice객체에 set해준다.
			Notif notice = new Notif();
			notice.setId(id);
			notice.setWriter(writer);
			notice.setTitle(title);
			notice.setDate(date);
			notice.setContent(content);
			notice.setWriter(writer);
			notice.setViewcnt(viewcnt);
			
			// jquery
			request.setAttribute("notice", notice);
	%>
	<div style="padding-top: 20px;">
		<table style="border-color: #FF337B">
			<tr>
				<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
				<td style="border-bottom-color: #FF337B" align="right">
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>2020년 1학기</option>
				</select>
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
				</select></td>
			</tr>
		</table>
	</div>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="viewInsertedOne.jsp" name="fr" onsubmit="return join()" enctype="multipart/form-data">
		<table border="0">	
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
			<col width="8%">	
		</colgroup>
			<tr>
				<th>년도/학기</th><td colspan="5">2020년도 1학기</td>
			</tr>
			<tr>
				<th>강의</th><td colspan="5">웹서버프로그래밍</td>
			</tr>
				
			<tr>
				<th>제목</th>
				<td colspan="5"><input type="text" value="${notice.title}" style="width: 350px;" name="title"></td>
			</tr>
			<tr>
				<th>작성일</th>
				<td><input type="hidden" value="${notice.date}" name="date" />${notice.date}</td>
		
				<th>작성자</th>
				<td><input type="hidden" value="${notice.writer}" name="writer">${notice.writer}</td>
			
				<th>조회수</th>
				<td><input type="hidden" value="${notice.viewcnt}" name="viewcnt">${notice.viewcnt}</td>
			</tr>
			<tr>
				<td colspan="6"><textarea cols="132" rows="16" style="overflow:auto;" name="content">${notice.content}</textarea></td>
			</tr>
			<tr>
				<th>추가 첨부파일</th>
				<td colspan="5"><input type="file" name="upload"/></td>
			</tr>
		</table>
		<input type="hidden" value="<%=rootid %>" name="rootid" />
		<input type="hidden" value="${notice.id}" name="id" />
		<div align="right">
		<input id="button" style="background-color: gray;" type="button" onclick="goBack();" value="취소"/>
		<input type="submit" id="button" value="수정"/>
		
		</div>
	</form>
</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->