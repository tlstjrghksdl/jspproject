<%@page import="dao.NotifDAO"%>
<%@page import="domain.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

	/* 값을 가지고 add 혹은 delete 페이지로 이동할 function */
	function goInsert() {
		var del = document.forms['inputform'];
		del.action = 'addCandidate.jsp';
		del.submit();
	}

	function goDelete() {
		var del = document.forms['inputform'];
		del.action = 'deleteNotice.jsp';
		del.submit();
	}
	
	
	
</script>
</head>
<body>
	<%
			String id = request.getParameter("id");
			String writer = request.getParameter("writer");
			String title = request.getParameter("title");
			String date = request.getParameter("date");
			String content = request.getParameter("content");
			String viewcnt = request.getParameter("viewcnt");
	
			Notif notice = new Notif();
			notice.setId(id);
			notice.setWriter(writer);
			notice.setTitle(title);
			notice.setDate(date);
			notice.setContent(content);
			notice.setWriter(writer);
			notice.setViewcnt(viewcnt);
			
			request.setAttribute("notice", notice);
	%>

	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="viewInsertedOne.jsp" id ="name1" name="inputform" onsubmit="return check()">
		<table>
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
			<col width="8%">	
		</colgroup>
		<tr>
			<th>년도/학기</th><td colspan="5">2020년도 1학기</td>
		</tr>
		<tr>
			<th>강의</th><td colspan="5">웹서버프로그래밍</td>
		</tr>
		<tr>
			<th>제목</th>
			<td colspan="5"><input type="text" style="width: 350px;" value="${notice.title}" name="title"></td>
		</tr>
		<tr>
			<th>작성일</th>
			<td><input type="hidden" value="${notice.date}" name="date">${notice.date}</td>
			<th>작성자</th>
			<td><input type="hidden" value="${notice.writer}" name="writer">${notice.writer}</td>
			<th>조회수</th>
			<td><input type="hidden" value="${notice.viewcnt}" name="viewcnt">${notice.viewcnt}</td>
		</tr>
		<tr>
			<td colspan="6" height="250px;"><textarea cols="132" rows="13" style="overflow:auto;" name="content"></textarea></td>
		</tr>
		<tr>
			<th height="50px;">첨부파일</th>
			<td colspan="5" height="50px;"><p style="margin: 0px">버추얼박스.PNG</p><p style="margin: 0px">8080.PNG</p><p style="margin: 0px">81.PNG</p></td>
		</tr>
		<input type="hidden" value="${notice.rootid}" name="rootid" />
		<input type="hidden" value="${notice.recnt}" name="recnt" />
		<input type="hidden" value="${notice.id}" name="id" /> <!-- 번호 -->
	</table>
		<div align="right">
		<a href="mainNotice.jsp"><input type="button" value="취소"/></a>
		<input type="submit" value="수정"/>
		<input type="button" onclick="goDelete();" value="삭제"/></a>
		</div>
	</form>

</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->