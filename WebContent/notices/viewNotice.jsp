<%@page import="domain.Comments"%>
<%@page import="domain.UploadFile"%>
<%@page import="dao.NotifDAO"%>
<%@page import="domain.Notif"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<!-- 스타일을 정해둔다. -->
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>

<script type="text/javascript">
//onclick함수를 만든다. 답변으로 가는 것과 제거로 가는 함수이다.
function goReply() {
	var del = document.forms['inputform'];
	del.action = 'replyNotice.jsp';
	del.submit();
}
function goDelete() {
	var del = document.forms['inputform'];
	del.action = 'deleteNotice.jsp';
	del.submit();
}
</script>
</head>
<body>
	<%
		// id를 전 jsp에서 받아온다.
		String id = request.getParameter("id");
		
		// 댓글 파라미터	
		String comment = request.getParameter("comment");
		String writer = request.getParameter("writer");
		List<Comments> comments = new ArrayList<>();
		
		// 파라미터로 받은 댓글과 작성자가 null이 아닐때만 실행한다.
		if(comment != null || writer != null) {
			
			NotifDAO.insertComment(id, comment, writer);
			
		} else {}
		
		// 댓글 select
		comments = NotifDAO.selectComment(id);
			
			
		
		int updateId = Integer.parseInt(id);
		NotifDAO.updateViewCnt(updateId);
		
		// 데이터 하나를 불러오는 메소드를 실행한다.
		Notif noticeOne = NotifDAO.selectOne(Integer.parseInt(id));
		List<UploadFile> ufs = NotifDAO.selectFile(Integer.parseInt(id));
		
		// jquery
		request.setAttribute("noticeOne", noticeOne);
	%>
	<div style="padding-top: 20px;"> 
		<table style="border-color: #FF337B">
			<tr>
				<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
				<td style="border-bottom-color: #FF337B" align="right">
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>2020년 1학기</option>
				</select>
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
				</select></td>
			</tr>
		</table>
	</div>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="updateNotice.jsp" id="inputform" onsubmit="return check()"  enctype="multipart/form-data">
	<table>
		<colgroup>
			<col width="15%">	
			<col width="25%">	
			<col width="15%">	
			<col width="25%">	
			<col width="12%">	
			<col width="8%">	
		</colgroup>
		<!-- 불러온 데이터를 UI에 표시하기위한 것 -->
		<tr>
			<th>년도/학기</th><td colspan="5">2020년도 1학기</td>
		</tr>
		<tr>
			<th>강의</th><td colspan="5">웹서버프로그래밍</td>
		</tr>
		<tr>
			<th>제목</th>
			<td  colspan="5"><input type="hidden" value="${noticeOne.title}" name="title">${noticeOne.title}</td>
		</tr>
		<tr>
			<th>작성일</th>
			<td><input type="hidden" value="${noticeOne.date}" name="date">${noticeOne.date}</td>
			<th>작성자</th>
			<td><input type="hidden" value="${noticeOne.writer}" name="writer">${noticeOne.writer}</td>
			<th>조회수</th>
			<td><input type="hidden" value="${noticeOne.viewcnt}" name="viewcnt">${noticeOne.viewcnt}</td>
		</tr>
		<tr>
			<td colspan="6" height="250px;"><input type="hidden" value="${noticeOne.content}" name="content">${noticeOne.content}</td>
		</tr>
		<tr>
			<th height="50px;">첨부파일</th>
			<td colspan="5" height="50px;">
				<%
				for(int i = 0; i < ufs.size(); i++) {
					if(ufs.get(i).getFilename().equals("")) {
						continue;
					}%>
				<a href="filedown.jsp?file=<%=ufs.get(i).getFilename()%>"><%=ufs.get(i).getFilename()%></a><br>
			<%} %>
			</td>
		</tr>
		<!-- UI에는 안보이지만 파라미터로 넘겨줄 값들을 hidden으로 숨겨둔다. -->
	</table>
		<input type="hidden" value="${noticeOne.rootid}" name="rootid" />
		<input type="hidden" value="${noticeOne.relevel}" name="relevel" />
		<input type="hidden" value="${noticeOne.recnt}" name="recnt" />
		<input type="hidden" value="${noticeOne.id}" name="id" />
		<div align="right">
		<!-- 목록을 눌렀을때, 목록을 보여주는 jsp로 이동한다. -->
		<a href="mainNotice.jsp"><input type="button" id="button" style="background-color: gray;" value="목록"/></a>
		<!-- 답변을 클릭하면 답변 jsp로 이동한다. -->
		<input type="button" id="inputform" onclick="goReply();" style="background-color: #FF337B; color: white; width: 75px; height: 30px;" value="답글"/>
		<!-- 수정을 누르면 submit이 동작하면서 수정 jsp로 이동한다. -->
		<input type="submit" id="button" value="수정"/>
		<!-- 삭제를 클릭하면 삭제 jsp로 이동한다. -->
		<input type="button" onclick="goDelete();" id="button" value="삭제"/>
		</div>
	</form> 
	<br>
	<br>
	<form method="post" action="viewNotice.jsp">
		<table>
			<tr >
				<td colspan="5" rowspan="2"><textarea rows="3" cols="120" name="comment" ></textarea></td>
				<td><input type="text" value="작성자" name="writer" required/></td>
				
			</tr>
			<tr>
				<td align="left"><input type="submit" value="댓글달기▶" /></td>
			</tr>
			<%for(int i = 0; i < comments.size(); i++) { 
				
				if(comments.size() == 0) {
					break;
				}%>
			<tr>
				<td width="80"><%=comments.get(i).getWriter() %></td>
				<td colspan="5" align="left"><%=comments.get(i).getComment() %></td>
			</tr>
			<%} %>
		</table>
		<input type="hidden" value="${noticeOne.title}" name="title"> 
		<input type="hidden" value="${noticeOne.date}" name="date">
		<input type="hidden" value="${noticeOne.writer}" name="writer">
		<input type="hidden" value="${noticeOne.viewcnt}" name="viewcnt">
		<input type="hidden" value="${noticeOne.content}" name="content">
		<input type="hidden" value="${noticeOne.rootid}" name="rootid" />
		<input type="hidden" value="${noticeOne.relevel}" name="relevel" />
		<input type="hidden" value="${noticeOne.recnt}" name="recnt" />
		<input type="hidden" value="${noticeOne.id}" name="id" />
	</form>

</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->