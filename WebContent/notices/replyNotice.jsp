<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="domain.Notif"%>
<%@page import="dao.NotifDAO"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-bottom: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>

<script type="text/javascript">

	function join(){

		var title = document.fr.title.value;

		var newtitle = title.replace(/(\s*)/g, "");
		if(newtitle.length == 0) {
			alert("공백입니다. 입력해주세요.")
			document.fr.title.focus();
			return false;
		}
	}
</script>
</head>
<body>
	<%	// 업로드한 폴더의 위치와 업로드 폴더의 이름을 알아야 한다.
		String savePath ="upload";// WebContent/uploadFile
		// 위의 폴더는 상대경로이고 절대경로 기준의 진짜 경로를 구해와야한다.
		String uploadPath = request.getSession().getServletContext().getRealPath(savePath);		// 업로드 경로
		int maxFileSize = 1024 * 1024 * 2;	// 업로드 제한 용량 = 2MB
		String encoding = "utf-8";			// 인코딩
		
		// 파일 업로드를 위한 MultipartRequest
		MultipartRequest multi = new MultipartRequest(
			request, 
			uploadPath, 
			maxFileSize, 
			encoding, 
		new DefaultFileRenamePolicy());
		
		// 받아온 파라미터를 변수로 초기화
		String id =     multi.getParameter("id");
		String writer = multi.getParameter("writer");
		String title =  multi.getParameter("title");
		String date =   multi.getParameter("date");
		String content= multi.getParameter("content");
		String rootid = multi.getParameter("rootid");
		String relevel= multi.getParameter("relevel");
		String recnt =  multi.getParameter("recnt");
		String viewcnt= multi.getParameter("viewcnt");
		// 답글 달 글의 relevel에서 1더한다.
		int inputRelevel = Integer.parseInt(relevel) + 1;
		int recnts = NotifDAO.getRecnt(rootid, Integer.toString(inputRelevel))+1;
	
	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		int getNum = NotifDAO.getNum() + 1;
		request.setAttribute("getNum", getNum);
		request.setAttribute("date", sdf.format(calendar.getTime()));
		request.setAttribute("id", id);
		request.setAttribute("num", inputRelevel);
		request.setAttribute("recnt", recnts);

		%>
		<div style="padding-top: 20px;">
			<table style="border-color: #FF337B">
				<tr>
					<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
					<td style="border-bottom-color: #FF337B" align="right">
					<select style="height: 25px">
					<option value="none" selected="selected" disabled>2020년 1학기</option>
					</select>
					<select style="height: 25px">
					<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
					</select></td>
				</tr>
			</table>
		</div>
		<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
		<form method="post" action="viewInsertedOne.jsp" name="fr" onsubmit="return join()" enctype="multipart/form-data">
			<table>	
				<tr>
					<th>번호</th>
					<td><input type="hidden" value="${getNum}" name="id" />답글신규(insert)</td>
				</tr>
				<tr>
					<th>작성자</th>
					<td><input type="text" style="width: 200px;" name="writer"></td>
				</tr>
				<tr>
				<tr>
					<th>제목</th>
					<td><input type="text" style="width: 350px;" name="title"></td>
				</tr>
				<tr>
					<th>일자</th>
					<td><input type="hidden" value="${date}" name="date" />${date}</td>
				</tr>
				<tr>
					<th>내용</th>
					<td><textarea cols="132" rows="13" style="overflow:auto;" name="content"></textarea></td>
				</tr>
			</table>
			<input type="hidden" value="<%=rootid %>" name="rootid"/>
			<input type="hidden" value="${num}" name="relevel"/>
			<input type="hidden" value="${recnt}" name="recnt" />
			<br>
			<div align="right"><a href="mainNotice.jsp"><input type="button" value="취소"/></a>
			<input type="submit" id="name" value="쓰기" /></div>
		</form>
	
	</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->