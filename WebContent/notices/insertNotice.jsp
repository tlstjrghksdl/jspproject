<%@page import="dao.NotifDAO"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%
	request.setCharacterEncoding("utf-8");
%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">

<title></title>
<style type="text/css">
	table {
		width: 1000px;
		border-top: 1px solid #444444;
		border-bottom: 1px solid #444444;
		border-collapse: collapse;
		margin-bottom: 10px;
	}
	
	th, td {
		border-bottom: 1px solid #444444;
		padding: 5px;
	}
	div {
		width: 1000px;	
	}
	
	#none {
		border-bottom: none;
		border-top: none;
	}
	
	#button {
		background-color: #FF337B;
		color: white;
		width: 75px;
		height: 30px;
	}
</style>

<script type="text/javascript">
// 제목의 공백을 잡아주는 함수
function join(){

	var title = document.fr.title.value;
	var newtitle = title.replace(/(\s*)/g, "");
	if(newtitle.length == 0) {
		alert("공백입니다. 입력해주세요.")
		document.fr.title.focus();
		return false;
	}
}
	
</script>
</head>
<body>
	<%	
		// 날짜를 입력하기 위한 simpledateformat, calendar
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		
		// 몇번째 글인지 num을 구한다.
		int getNum = NotifDAO.getNum() + 1;
		
		//jquery
		request.setAttribute("getNum", getNum);
		request.setAttribute("date", sdf.format(calendar.getTime()));
	%>
	
	<!-- 글쓰기를 눌렀을 때, 다음 jsp로 넘어갈 파라미터들을 post방식으로 보낸다.
		name을 지정해주고, UI에 안보여질 애들은 hidden처리한다. -->
	
	
	<div style="padding-top: 20px;">
		<table style="border-color: #FF337B">
			<tr>
				<td style="border-bottom-color: #FF337B; font-size: 20px; color: #FF337B "><b>웹서버프로그래밍</b></td>
				<td style="border-bottom-color: #FF337B" align="right">
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>2020년 1학기</option>
				</select>
				<select style="height: 25px">
				<option value="none" selected="selected" disabled>웹서버프로그래밍</option>
				</select></td>
			</tr>
		</table>
	</div>
	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post" action="viewInsertedOne.jsp" name="fr" onsubmit="return join()" enctype="multipart/form-data">
		<table>	
			<tr>
				<th>번호</th>
				<td><input type="hidden" value="${getNum}" name="id" />신규(insert)</td>
			</tr>
			<tr>
				<th>작성자</th>
				<td><input type="text" style="width: 200px;" value="${getWriter}" name="writer" /></td>
			</tr>
			<tr>
				<th>제목</th>
				<td><input type="text" style="width: 350px;" name="title"></td>
			</tr>
			<tr>
				<th>일자</th>
				<td><input type="hidden" value="${date}" name="date" />${date}</td>
			</tr>
			<tr>
				<th>내용</th>
				<td><textarea cols="132" rows="13"style="overflow:auto;"  name="content"></textarea></td>
			</tr>
			<tr>
				<th>첨부파일</th>
				<td><input type="file" name="upload"/></td>
			</tr>
		</table>
			<input type="hidden" value="${getNum}" name="rootid" />
			<input type="hidden" value="0" name="relevel"/>
		<br>
		<div align="right"><a href="mainNotice.jsp"><input type="button" id="button" style="background-color: gray;" value="취소"/></a>
		<input type="submit" id="button" value="쓰기"/></div>
	</form>

</body>
</html><!-- 
<div style="overflow: auto; width: 500px; height: 500px;"></div> -->