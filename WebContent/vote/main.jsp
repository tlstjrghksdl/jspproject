<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.VoteMethod"%>
<%@page import="domain.Candidate"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<script type="text/javascript">

	/* 값을 가지고 add 혹은 delete 페이지로 이동할 function */
	function goInsert() {
		var del = document.forms['inputform'];
		del.action = 'addCandidate.jsp';
		del.submit();
	}

	function goDelete() {
		var del = document.forms['inputform'];
		del.action = 'deleteCandidate.jsp';
		del.submit();
	}
	
</script>
</head>
<body>
	<%	// selectAll메소드를 실행하면 List<Candidate>를 리턴한다. 그래서 cdds에 담아주면된다.
		List<Candidate> cdds = VoteMethod.selectAll();
	%>

	<!-- form을 통해 정보를 취합하여 전해주기 때문에 post사용 -->
	<form method="post">
	
		<!-- 각각의 버튼을 눌렀을 때 다른 jsp로 이동을 위해 onclick을 사용 -->
		<input type="button" style="background-color: yellow;" onclick="location.href='main.jsp'" value="후보등록" /> 
		<input	type="button"  onclick="location.href='vote.jsp'" value="투표" /> 
		<input type="button" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	<form method="post" action="deleteCandidate.jsp" name="inputform">
		<table border="1">

			<%	// for문을 돌면서 select한 값을 표에 출력한다.
				for (int i = 0; i < cdds.size(); i++) {
			%>
			<tr>
				<td>기호번호 : <input type="text" value="<%=cdds.get(i).getNum() %>" name="id1" readonly /></td>

				<td>후보명 : <input type="text" value="<%=cdds.get(i).getName() %>" name="name1" readonly /></td>
				
				<!-- 삭제버튼을 누르면 delete function이 실행되게 한다. -->
				<td><a href="./deleteCandidate.jsp?id1=<%=cdds.get(i).getNum()%>" ><input type="button" value="삭제" style="background: lime;"></a></td>
			</tr>
			<%
				}
			%>
			<!-- 후보자 추가 -->
			<tr>
				<td>기호번호 : <input type="number" name="id"></td>
				<td>후보명 : <input type="text" name="name"></td>
				<td><input style="background-color:lime;" type="button"
					onclick="goInsert();" value="추가" /></td>
			</tr>
		</table>
	</form>

</body>
</html>