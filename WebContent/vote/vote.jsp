<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.VoteMethod"%>
<%@page import="domain.Candidate"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>

<script type="text/javascript">
	var select = document.getElementById("selected");
	var value = select.options[select.selectedIndex].value;
	alert(value);
</script>
</head>
<body>
	<%
		// 후보자를 selectAll한다.
		List<Candidate> cdds = VoteMethod.selectAll();
	%>



	<form>
		<input type="button" onclick="location.href='main.jsp'" value="후보등록" />
		<input type="button" style="background-color: yellow;" onclick="location.href='vote.jsp'" value="투표" />
		<input type="button" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	<form method="post" action="insertVote.jsp">
	<table border="1">
	<tr>
	<td>
	<!-- select로 선택할 수 있는 form을 만들었다. 선택은 사용할 수 없게 하고 맨 처음 고정으로 해준다. -->
		<select name="selectNum" size="1">
			<option value="none" selected="selected" disabled>=== 선택 ===</option>
			<%for(int i = 0; i < cdds.size(); i++) { %>
				<option value="<%=cdds.get(i).getNum()%>"><%=cdds.get(i).getNum()%>번 <%=cdds.get(i).getName() %></option>
			<%} %>			
		</select>
	</td>
	<td>
		<select name="ages" size="1">
			<option value="none" selected="selected" disabled>=== 선택 ===</option>
			<%for(int i = 1; i < 10; i++) { %>
				<option value="<%=i * 10 %>" ><%=i * 10 %>대</option>
			<%} %>
		</select>
	</td>
		
	<td><input type="submit" value="투표하기" onclick="location.href='insertVote.jsp'"/></td>
	</tr>
		
		
		
		</table>
	</form>
</body>
</html>