<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.VoteMethod" %>
<%@page import="domain.Candidate"%>
<%@page import="domain.VotePeople"%>
<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>	
	<%		
		int num = Integer.parseInt(request.getParameter("selectNum"));
		int age = Integer.parseInt(request.getParameter("ages"));
		VotePeople vp = new VotePeople(num, age);
		VoteMethod.insertVote(vp);
	%>


	<form method="post">
		<input type="button" style="background-color: yellow;" onclick="location.href='main.jsp'" value="후보등록" /> 
		<input	type="button"  onclick="location.href='vote.jsp'" value="투표" /> 
		<input type="button" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	투표 결과 : 투표하였습니다.
</body>
</html>