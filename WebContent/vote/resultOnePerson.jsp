<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.VoteMethod"%>
<%@page import="domain.Candidate"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<style type="text/css">
	table {
		width: 500px;
	}
</style>
</head>
<body>
	
	<%	// vote.jsp에서 받아온 숫자.
		String Num = request.getParameter("id");
		// int로 형변환한다.
		int inputNum = Integer.parseInt(Num);
		// selectAll실행.
		List<Candidate> cdds = VoteMethod.selectAll();
	%>







	<form method="post">
		<input type="button" onclick="location.href='main.jsp'" value="후보등록" />
		 <input type="button" onclick="location.href='vote.jsp'" value="투표" /> 
		 <input type="button" style="background-color: yellow;" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	<table border="1">
		<colgroup>
			<col width="20%">
			<col width="80%"> 
		</colgroup>
	<% 
	out.println(inputNum + ". " + cdds.get(inputNum-1).getName() + "후보 득표성향 분석");	
	// 각 연령대별로 나와야 하기 때문에 일단 연령대를 표시하고, 
	// viewResultOne메소드를 실행시키면 return되는 값을 테이블에 나타낸다. 
	for(int i = 0; i < 9; i++) { %>
		<tr>
		<td><%= (i+1)*10 %>대</td>
		<td><%= VoteMethod.viewResultOne(inputNum, (i+1)*10) %></td>
		</tr>
		<%} %>
	</table>
</body>
</html>