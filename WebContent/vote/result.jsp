<%@page import="domain.VotePeople"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.VoteMethod"%>
<%@page import="domain.Candidate"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<style type="text/css">
	table {
		width: 500px;
	}
</style>
</head>
<body>
	<%	// 한명한명 결과 확인을 해야하기 때문에 일단 selectAll메소드를 실행한다.
		List<Candidate> cdds = VoteMethod.selectAll();
	%>
	<form method="post">
		<input type="button" onclick="location.href='main.jsp'" value="후보등록" />
		 <input type="button" onclick="location.href='vote.jsp'" value="투표" /> 
		 <input type="button" style="background-color: yellow;" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	<table border="1">
		<colgroup>
			<col width="20%">
			<col width="80%"> 
		</colgroup>
		<%
			for (int i = 0; i < cdds.size(); i++) {
		%>
		<tr> <!-- 후보자 명을 클릭했을 때 그 후보자를 투표한 연령대를 보여줘야 하기 때문에 a태그로 연결시킨다. -->
			<td><a href="./resultOnePerson.jsp?id=<%=cdds.get(i).getNum()%>"><%=cdds.get(i).getNum()%> <%=cdds.get(i).getName()%></a></td>
			<!-- 투표갯수를 출력해주는 메소드를 써준다. -->
			<td><%=VoteMethod.viewResult(cdds.get(i))%></td>
		</tr>
		<%
			}
		%>
	</table>

</body>
</html>