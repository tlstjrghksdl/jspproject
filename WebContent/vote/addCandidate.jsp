
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@page import="dao.VoteMethod"%>
<%@page import="domain.Candidate"%>
<% request.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title></title>
</head>
<body>	
	<%	
		// 받아온 값을 int형변환후 변수에 담는다. 그리고 만들어진 후보자 객체에 set해주고
		// insertCandidate메소드를 실행할 때 필요한 매개변수로 넣어주면 insert된다.
		Candidate candidate = new Candidate();
		int num = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		candidate.setNum(num);
		candidate.setName(name);
		VoteMethod.insertCandidate(candidate);
	%>


	<form method="post">
		<input type="button" style="background-color: yellow;" onclick="location.href='main.jsp'" value="후보등록" /> 
		<input	type="button"  onclick="location.href='vote.jsp'" value="투표" /> 
		<input type="button" onclick="location.href='result.jsp'" value="개표결과" />
	</form>
	후보 등록 결과 : 후보가 추가(삭제) 되었습니다.
</body>
</html>