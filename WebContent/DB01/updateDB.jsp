<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@page import="dao.StudentDAO"%>
<%@page import="domain.Student"%>
<%
	request.setCharacterEncoding("utf-8");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>학생 점수 표</title>

<style>
table {
	font-family: monospace;
	font-size: 100%;
	width: 450px;
}

#bgcol {
	background-color: yellow;
}
}
</style>
</head>
<body>

	<%
	
	// 받아온 값들을 student객체에 담는다.
	String name1 = request.getParameter("name");
	String id1 = request.getParameter("id2");
	String kor1 = request.getParameter("kor");
	String eng1 = request.getParameter("eng");
	String mat1 = request.getParameter("mat");
	String compare = "";
	Student st = new Student();
	st.setName(name1);
	st.setId(id1);
	st.setKor(kor1);
	st.setEng(eng1);
	st.setMat(mat1);
	
	//그리고 값들을 담은 student객체를 update메소드에 매개변수로 넣고 실행시켜 update가 되게 한다.
	StudentDAO.update(st);
	
	// updqte됐는지 확인하기 위해 selectAll메소드를 실행시켜 return된 값을 List에 담는다.
	List<Student> students = StudentDAO.selectAll();
	%>

	<!-- 테이블 만들기 -->
	<h1>레코드 수정 완료</h1>
	<table border="1">
		<!-- colgroup으로 표 각 셀의 너비를 정해준다. -->
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
			<col width="20%">
		<tr>
			<!-- 맨위에 출력될 것 -->
			<td align="center">이름</td>
			<td align="center">학번</td>
			<td align="center">국어</td>
			<td align="center">영어</td>
			<td align="center">수학</td>
		</tr>
		<!-- for문으로 얻은 값들을 하나씩 출력하게 한다. -->
		<%	// update할때 가져온 id와 같은놈은 노란색으로 체크되도록 해준다.
			for (int i = 0; i < students.size(); i++) {
			if (students.get(i).getId().equals(id1)) {
		%>
		<tr id="bgcol">
			<!-- a태그 href로 이름을 request.getParameter할 수 있게 지정해준다. 이름을 누르면 그 이름의 데이터를 출력해야하기때문에 name으로 잡는다. -->
			<td align="center"><%=students.get(i).getName()%></td>
			<td align="center"><%=students.get(i).getId()%></td>
			<td align="center"><%=students.get(i).getKor()%></td>
			<td align="center"><%=students.get(i).getEng()%></td>
			<td align="center"><%=students.get(i).getMat()%></td>
		</tr>
		<%
			} else {
		%>

		<tr>
			<!-- a태그 href로 이름을 request.getParameter할 수 있게 지정해준다. 이름을 누르면 그 이름의 데이터를 출력해야하기때문에 name으로 잡는다. -->
			<td align="center"><%=students.get(i).getName()%></td>
			<td align="center"><%=students.get(i).getId()%></td>
			<td align="center"><%=students.get(i).getKor()%></td>
			<td align="center"><%=students.get(i).getEng()%></td>
			<td align="center"><%=students.get(i).getMat()%></td>
		</tr>
		<%
			}
		}
		%>

	</table>
</body>
</html>