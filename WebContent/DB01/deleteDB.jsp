<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@page import="dao.StudentDAO"%>
<%@page import="domain.Student"%>


<% request.setCharacterEncoding("utf-8"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>학생 점수 표</title>

<style>
/* 테이블의 스타일을 정해준다. */
	table {
		font-family: monospace;
		font-size: 100%;
		width: 450px;
	}
</style>
</head>
<body>

<%
	
	// 전 페이지에서 얻은 이름, id, 점수를 String변수에 담고, Student객체에 넣어준다.
	String name1 = request.getParameter("name");
	String id1 = request.getParameter("id2");
	String kor1 = request.getParameter("kor");
	String eng1 = request.getParameter("eng");
	String mat1 = request.getParameter("mat"); 
	Student std = new Student();
	std.setName(name1);
	std.setId(id1);
	std.setKor(kor1);
	std.setEng(eng1);
	std.setMat(mat1);
	
	// 그 객체를 delete메소드에 매개변수로 넣는다.
	StudentDAO.delete(std);
	
	// delete가 잘 됐는지 출력해봐야하기때문에 selectAll메소드를 불러온다.
	List<Student> students = StudentDAO.selectAll();
%>

<!-- 테이블 만들기 -->
<h1>레코드 수정 완료</h1>
<table border="1">
<!-- colgroup으로 표 각 셀의 너비를 정해준다. -->
	<colgroup>
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
	<tr>
	<!-- 맨위에 출력될 것 -->
	<td align="center">이름</td>
	<td align="center">학번</td>
	<td align="center">국어</td>
	<td align="center">영어</td>
	<td align="center">수학</td>
	</tr>
		<!-- for문으로 얻은 값들을 하나씩 출력하게 한다. -->
		<% for ( int i = 0; i < students.size(); i++) { %>
		<tr>
		<!-- a태그 href로 이름을 request.getParameter할 수 있게 지정해준다. 이름을 누르면 그 이름의 데이터를 출력해야하기때문에 name으로 잡는다. -->
		<td align="center"><%= students.get(i).getName() %></td>
		<td align="center"><%= students.get(i).getId()   %></td>
		<td align="center"><%= students.get(i).getKor()  %></td>
		<td align="center"><%= students.get(i).getEng()  %></td>
		<td align="center"><%= students.get(i).getMat()  %></td>
		</tr>
		<%} %>	
	
</table>
</body>
</html>