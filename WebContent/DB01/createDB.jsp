<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="dao.StudentDAO" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>create</title>
</head>
<body>

<%
	// create메소드 불러오면 끝!
	StudentDAO.create();
%>

<!-- 완료되면 출력한다. -->
<h1>create 완료</h1>
</body>
</html>