<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="dao.StudentDAO" %>
<%@ page import="domain.Student"%>

<% request.setCharacterEncoding("utf-8"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insertDB</title>
<style>
	table {
		width: 450px;
		font-family: monospace;
		font-size: 100%;
	}
	table, td {
		text-align: center;
	}
</style>
</head>
<body>
	<%
			// 받아온 값들을 student 객체에 담아주고 insertDB메소드를 실행시킨다.
			String name = request.getParameter("name");
			String id = StudentDAO.getBlankNum();
			String kor = request.getParameter("kor");
			String eng = request.getParameter("eng");
			String mat = request.getParameter("mat");
			Student st = new Student();
			st.setName(name);
			st.setId(id);
			st.setKor(kor);
			st.setEng(eng);
			st.setMat(mat);
			StudentDAO.insertDB(st);
			
	%>
	
	
	<h1>성적추가완료</h1>

	<input type="button" value="뒤로가기" onclick="history.back(-1);" />

	
	<table border="1">
	<colgroup>
	<col width="20%">
	<col width="80%">
	</colgroup>
	<tr>
	<td>이름</td>
	<td><input type="text" value="<%=st.getName() %>" /></td>
	</tr>
	<tr>
	<td>학번</td>
	<td><input type="text" value="<%=st.getId() %>" /></td>
	</tr>
	<tr>
	<td>국어</td>
	<td><input type="text" value="<%=st.getKor() %>" /></td>
	</tr>
	<tr>
	<td>영어</td>
	<td><input type="text" value="<%=st.getEng() %>" /></td>
	</tr>
	<tr>
	<td>수학</td>
	<td><input type="text" value="<%=st.getMat() %>" /></td>
	</tr>
	</table>
	</body>
</html>