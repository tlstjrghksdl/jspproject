<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="dao.StudentDAO"%>
<%@ page import="domain.Student"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OneView</title>

<style>
table {
	font-family: monospace;
	font-size: 100%;
	width: 500px;
}
</style>
</head>
<body>

	<%	
	
		// 평균을 소수점 두자리로 하기 위해 DecimalFormat을 만든다
		DecimalFormat df = new DecimalFormat("0.##");
		// 받아온 id값
		String id1 = request.getParameter("id");
		// 받아온 id값을 selectOne메소드에 매개변수로 담아서 실행하면 Student객체에 담겨진다.
		Student std = StudentDAO.selectOne(id1);
		
		// 합계와 평균을 구하기 위한 것들
		int korScore = Integer.parseInt(std.getKor());
		int engScore = Integer.parseInt(std.getEng());
		int matScore = Integer.parseInt(std.getMat());
		int sum = korScore + engScore + matScore;
		String avg = df.format((double) sum / 3);
	%>
	<!-- table 만들기 -->
	<h1>
		[<%=std.getName()%>]조회
	</h1>
	<table border="1">
		<!-- colgroup을 통해 각 셀의 너비를 정해준다. -->
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="10%">
			<col width="10%">
			<col width="10%">
			<col width="15%">
			<col width="15%">
			<!-- 제목은 회색으로 한다. -->
		<tr bgcolor="gray">
			<td align="center"><b>이름</b></td>
			<td align="center"><b>학번</b></td>
			<td align="center"><b>국어</b></td>
			<td align="center"><b>영어</b></td>
			<td align="center"><b>수학</b></td>
			<td align="center"><b>합계</b></td>
			<td align="center"><b>평균</b></td>
		</tr>
		<!-- 한명만 출력할 것이기 때문에 for문은 필요없다.
	 받아온 값은 무조건 첫번째 요소에 저장되기 때문에  첫번째 요소만 출력해주면 된다.-->
		<tr>
			<td align="center"><%=std.getName() %></td>
			<td align="center"><%=std.getId()   %></td>
			<td align="center"><%=std.getKor()  %></td>
			<td align="center"><%=std.getEng()  %></td>
			<td align="center"><%=std.getMat()  %></td>
			<td align="center"><%=sum %></td>
			<td align="center"><%=avg %></td>
		</tr>
	</table>
</body>
</html>