<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="dao.StudentDAO"%>
<%@ page import="domain.Student"%>
<%@page import="domain.Paging"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>학생 점수 표</title>

<style>
/* 테이블의 스타일을 정해준다. */
table {
	font-family: monospace;
	font-size: 100%;
	width: 500px;
	
}
</style>
</head>
<body>

	<%
	
		// 평균을 소수점 2개까지 출력하기 위해 DecimalFormat으로 출력값을 정의해준다.
		DecimalFormat df = new DecimalFormat("0.##");
	
		// selectAll메소드를 실행하면 List<Student>가 return된다. 그것을 담는다.
		List<Student> students = StudentDAO.selectAll();

		
		// dataCnt는 학생의 수이다.
		int dataCnt = students.size();
		
		// Pagenation하기 위해 Paging 클래스를 객체화한다.
		Paging p = new Paging();
		
		// p객체에 학생수를 담는다.
		p.setTotalCount(dataCnt);
		
		// 현재 페이지 받아온 pg의 값이 null이 아니면 그대로 표시하고 null이면 1로 한다 즉 현재페이지 null이면 1페이지
		int cpage = request.getParameter("pg") != null ? Integer.parseInt(request.getParameter("pg")) : 1;
		int prevPage = (int) Math.floor((cpage - 1) / p.getPageSize()) * p.getPageSize();
		int nextPage = prevPage + p.getPageSize() + 1;
		p.setPageNo(cpage);
		p.setPrevPageNo(prevPage);
		p.setNextPageNo(nextPage);

		
		// url에서 from의 값을 받아서 from에 저장
		String from = request.getParameter("from");
		// url에서 cnt의 값을 받아서 cnt에 저장 
		String cnt = request.getParameter("cnt");

		// url에서 받아온 값을 int로 변환하여 저장할 변수
		int fromPT;
		int cntPT;

		// url에서 from이나 cnt의 값이 없으면 (첫페이지 처리 위함)
		if (from == null || cnt == null) {
			// fromPT=0, cntPT=10인 것으로 판단
			fromPT = 0;
			cntPT = 10;
		} else {
			
			// 마지막 페이지에서 표시할 데이터 수를 정하기 위한 if문이다. 현재 페이지가 마지막페이지랑 같다면,
			if(cpage == p.getFinalPageNo()) {
				// 데이터수가 꼭 10개가 아니기 때문에 총 데이터에서 10을 나눈 나머지값을 표시할 것이다.
				fromPT = Integer.parseInt(from);
				cntPT = dataCnt % 10;				
			}
			fromPT = Integer.parseInt(from);
			cntPT = Integer.parseInt(cnt);
		}		
	%>

	<!-- 테이블 만들기 -->
	<h1>조회</h1>

	<table border="1" ceellspacing="0">
		<!-- colgroup으로 표 각 셀의 너비를 정해준다. -->
		<colgroup>
			<col width="20%">
			<col width="20%">
			<col width="10%">
			<col width="10%">
			<col width="10%">
			<col width="10%">
			<col width="10%">
			<col width="10%">
		</colgroup>	
		<tr bgcolor="gray" align=center>
			<td align="center"><b>이름</b></td>
			<td align="center"><b>학번</b></td>
			<td align="center"><b>국어</b></td>
			<td align="center"><b>영어</b></td>
			<td align="center"><b>수학</b></td>
			<td align="center"><b>합계</b></td>
			<td align="center"><b>평균</b></td>
			<td align="center"><b>순위</b></td>
		</tr>
		<!-- for문으로 얻은 값들을 하나씩 출력하게 한다. -->
		<%
			for (int i = fromPT; i < fromPT + cntPT; i++) {
			double avg = Double.parseDouble(students.get(i).getAvg());
			String printAvg = df.format(avg);	
		%>
		<tr align=center>
			<!-- a태그 href로 이름을 request.getParameter할 수 있게 지정해준다. 이름을 누르면 그 이름의 데이터를 출력해야하기때문에 name으로 잡는다. -->
			<td align="center"><a
				href="./selectView.jsp?id=<%=students.get(i).getId() %>"><%=students.get(i).getName()%></a></td>
			<td align="center"><%=students.get(i).getId()  %></td>
			<td align="center"><%=students.get(i).getKor() %></td>
			<td align="center"><%=students.get(i).getEng() %></td>
			<td align="center"><%=students.get(i).getMat() %></td>
			<td align="center"><%=students.get(i).getSum() %></td>
			<td align="center"><%=printAvg %></td>
			<td align="center"><%=students.get(i).getRank() %></td>
		</tr>
		<%}	%>
	</table>
	
	
	<!-- 페이지 처리 -->
	<table style='margin-left: auto; margin-right: auto; table-layout: fixed;'>
		<tr>
			<td align="center">
				<%
				// 다음 페이지가 마지막 페이지가 아니라면
				if (p.getNextPageNo() > p.getFinalPageNo()) {
					
					// 현재 페이지 11로 나눈 값이 0이 아니라면, 11로 나눈 값이 0이라는 것은 1~10페이지를 나타낸다. 
					// 맨 처음 부분은 <<, < 가 필요 없다.
					// 즉, 1~10페이지가 아니면~ <<, < 표시
					if (p.getPageNo() / 11 != 0) {%> 
					<a href="Allview.jsp"><<</a> 
					<a href="Allview.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=p.getPrevPageNo() * 10%>&cnt=10"><</a> <%
					}
					
					// 현재페이지부터 마지막 페이지 전까지 실행하는 for문
					// i*10 - 10은 현재페이지가 1이면 0이되면서 0~10까지의 데이터를 나타내는 곳으로 이동.
					// 만약에 총 데이터수가 10으로 나눳을때 나머지가 없다면, 마지막 페이지에도 데이터를 10개 출력하도록 if문을 넣어준다.
					for (int i = 1 + p.getPrevPageNo(); i <= p.getFinalPageNo() - 1; i++) {%>
				 		<a href="Allview.jsp?pg=<%=i%>&from=<%=(i * 10 - 10)%>&cnt=10"> <%=i%> </a> 
				 	<%} if(dataCnt % 10 == 0) {%>
						<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=10"><%=p.getFinalPageNo() %></a>
				 	<%} else {%>
				 		<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=<%=dataCnt % 10 %>"><%=p.getFinalPageNo() %></a>
				 	<% }
				} else {
					// 다음 페이지가 마지막 페이지일때,
					// 페이지가 1~10이 아닐때 <<, <를 나타낸다.
					if (p.getPageNo() / 11 != 0) {%> 
						<a href="Allview.jsp"><<</a> 
						<a href="Allview.jsp?pg=<%=p.getPrevPageNo()%>&from=<%=p.getPrevPageNo() * 10%>&cnt=10"><</a>
					<% }
					// 현재페이지부터 마지막 페이지 전까지 실행하는 for문
					// i*10 - 10은 현재페이지가 1이면 0이되면서 0~10까지의 데이터를 나타내는 곳으로 이동.
					// 만약에 총 데이터수가 10으로 나눳을때 나머지가 없다면, 마지막 페이지에도 데이터를 10개 출력하도록 if문을 넣어준다.
					for (int i = 1 + p.getPrevPageNo(); i <= p.getFinalPageNo() - 1; i++) {%>
				 		<a href="Allview.jsp?pg=<%=i%>&from=<%=(i * 10 - 10)%>&cnt=10"> <%=i%> </a> 
				 	<%} if(dataCnt % 10 == 0) {%>
						<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=10"><%=p.getFinalPageNo() %></a>
				 	<%} else {%>
				 		<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=<%=dataCnt % 10 %>"><%=p.getFinalPageNo() %></a>
				 	<% }
					if (p.getPageNo() != p.getFinalPageNo()) {
						if(dataCnt % 10 == 0) {%>
							<a href="Allview.jsp?pg=<%=p.getNextPageNo()%>&from=<%=p.getNextPageNo() * 10%>&cnt=10">></a>
							<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=10">>></a>
						<% } else { %>
							<a href="Allview.jsp?pg=<%=p.getNextPageNo()%>&from=<%=p.getNextPageNo() * 10%>&cnt=10">></a>
							<a href="Allview.jsp?pg=<%=p.getFinalPageNo()%>&from=<%=p.getFinalPageNo() * 10 - 10%>&cnt=<%=dataCnt % 10 %>">>></a>
						<% }%> 
					
					<%}
				}%>
			</td>
		</tr>
	</table>
</body>
</html>