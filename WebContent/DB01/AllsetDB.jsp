<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="dao.StudentDAO" %>
<%@page import="domain.Student"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Allset</title>
</head>
<body>

<%	
		
		// 랜덤함수로 0~100까지의 점수가 나오게 하여 int값에 담는다.
		// Student객체를 인스턴스화 하여 그 값들을 넣어준다. 
		// 완성된 Student를 insertDB에 매개변수로 넣어준다.
		int a = (int)(Math.random()*100) + 1;
		int b = (int)(Math.random()*100) + 1;
		int c = (int)(Math.random()*100) + 1;
		Student student = new Student();
		student.setName("석환");
		student.setId("209930");
		student.setKor(Integer.toString(a));
		student.setEng(Integer.toString(b));
		student.setMat(Integer.toString(c));
		StudentDAO.insertDB(student);

%>

<h1>Allset 완료</h1>
</body>
</html>
