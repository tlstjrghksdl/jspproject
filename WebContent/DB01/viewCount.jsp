<%@page import="java.io.FileWriter"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="dao.StudentDAO"%>
<%@ page import="domain.Student"%>

<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<title>viewCount</title>
</head>
<body>
<h1 align="center">JSP Database 실습 1</h1>
		
		<%	
			// 메모장에 0을 입력하고 count로 저장한다.
			String data;
			//카운트 할 변수
			int count = 0;
			// 메모장에 0을 입력하고 count로 저장한다.
			FileReader fr = new FileReader("C:\\Users\\tjrgh\\Desktop\\count.txt");
			StringBuffer sb = new StringBuffer();
			
			// read를 받아올 int변수
			int ch = 0;
			
			// read해서 얻은 int형 자료를 char로 변경 후 StringBuffer에 append
			while((ch = fr.read()) != -1) {
				sb.append((char)ch);
			}
			
			// String data는 sb를 String형태로 바꾸고 빈칸을 없애고, enter도 없앤다.
			data = sb.toString().trim().replace("\n", "");
			fr.close();
			
			// count는 data로 int형변환 한다.
			count = Integer.parseInt(data);
			
			// count수 올리고, 다시 String 형으로 변환후 println으로 출력한다.
			count++;
			data = Integer.toString(count);
			
			out.println("<br><br>현재 홈페이지 방문조회수는 ["+ data +"] 입니다</br>");
			
			// data값이 1이 올라갔으니까 다시 count메모장에 입력한다.
			FileWriter fw = new FileWriter("C:\\Users\\tjrgh\\Desktop\\count.txt", false);
			fw.write(data);
			fw.close();
			
		%>	
</body>
</html>