<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@page import="dao.StudentDAO"%>
<%@ page import="domain.Student"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>게시판</title>

<style>
table {
	width: 450px;
	font-family: monospace;
	font-size: 100%;
}

table, td {
	text-align: center;
}
</style>
<script type="text/javascript">

/* 	updateDB.jsp, deleteDB.jsp로 이동하기 위한 function을 정의
	form에서 inputform을 불러오면 된다.
	check는 빈칸, 특수문자, 숫자를 없애기 위한 function인다.
*/
function goUpdate() {
    if (Check() == true) {
       var update = document.forms['inputform'];
       update.action = 'updateDB.jsp';
       update.submit();
    } else {
    }                     
 }

 function goDelete() {
    var del = document.forms['inputform'];
    del.action = 'deleteDB.jsp';
    del.submit();
 }

 function Check() {
    var stnName = document.getElementById("name").value;
    
    var blank = /\s+/g;
    var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
    var number = /[0-9]/;
    if (blank.test(stnName)) {
       alert('이름에 공백을 제거해 주세요');
       return false;            
    } else if (specialCharacters.test(stnName)) {
       alert('이름에 특수문자를 입력할 수 없습니다.');
       return false;
    } else if (stnName.length == 0){
       alert('빈 칸을 입력해 주세요.');
       return false;
    } else if (number.test(stnName)) {
       alert('이름에 숫자를 입력할 수 없습니다.');
       return false;               
    } else {
    }
    return true;
 }
</script>
</head>
<body>

	<%
	// 받아온 id값
	String id = request.getParameter("id");
	// Student를 객체화한다.
	Student std = new Student();
	// 객체화된 student에 selectOne메소드를 실행시켜 return한 값을 넣어준다.
	std = StudentDAO.selectOne(id);
	// student가 가진 이름이 null이면 알수없는 학번이라고 출력되게한다.
	if (std.getName() == null) {
	%>
	<h1>성적 조회후 정정 / 삭제</h1>
	<form method="post" name="myForm" action="showREC.jsp">
		조회할 학번 <input type="text" name="id"> <input type="submit"
			value="조회">
	</form>
	<form method="post" >
		<table border="1">

			<colgroup>
				<col width="20%">
				<col width="80%">
			</colgroup>


			<tr>
				<td>이름</td>
				<td><input type="text" value="알 수 없는 학번" /></td>
			</tr>
			<tr>
				<td>학번</td>
				<td><input type="text" value="" name="id2" readonly /></td>
			</tr>
			<tr>
				<td>국어</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0){this.value='0';}"
					value="" name="kor" /></td>
			</tr>
			<tr>
				<td>영어</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0){this.value='0';}"
					value="" name="eng" /></td>
			</tr>
			<tr>
				<td>수학</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0){this.value='0';}"
					value="" name="mat" /></td>
			</tr>
		</table>
	</form>
	<%	// 그게 아니라면 제대로 출력되게 한다.
		} else {
	%>
	<!-- table 만들기 -->
	<h1>성적 조회후 정정 / 삭제</h1>
	<form method="post" name="myForm" action="showREC.jsp">
		조회할 학번 <input type="text" name="id"> <input type="submit"
			value="조회">
	</form>
	<form method="post" name="inputform" id="inputform">
		<table border="1">
			<!-- colgroup을 통해 각 셀의 너비를 정해준다. -->
			<colgroup>
				<col width="20%">
				<col width="80%">
			</colgroup>

			<tr>
				<td>이름</td>
				<td><input id="name" type="text" value="<%=std.getName()%>"
					name="name" id="name" /></td>
			</tr>
			<tr>
				<td>학번</td>
				<td><input type="text" value="<%=std.getId()%>" name="id2"
					readonly /></td>
			</tr>
			<tr>
				<td>국어</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0 || this.value==''){this.value='0';}"
					value="<%=std.getKor()%>" name="kor" /></td>
			</tr>
			<tr>
				<td>영어</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0 || this.value==''){this.value='0';}"
					value="<%=std.getEng()%>" name="eng" /></td>
			</tr>
			<tr>
				<td>수학</td>
				<td><input type="number"
					onkeyup="if(this.value>100){this.value='100';}else if(this.value<0 || this.value==''){this.value='0';}"
					value="<%=std.getMat()%>" name="mat" /></td>
			</tr>
		</table>
		<!-- 이름이나 점수를 바꾸고 수정 버튼을 누르면 함수를 불러와서 값을 가지고 이동하게 된다. 
			삭제를 누르면 학번을 기준으로 학생이 삭제된다.-->
		<input type="button" value="수정" onclick="goUpdate();" /> <input
			type="button" value="삭제" onclick="goDelete();" />
	</form>
	<%
		}
	%>
</body>
</html>